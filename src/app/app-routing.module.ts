import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BuypageComponent} from './buypage/buypage.component';
import {PositionsComponent} from './positions/positions.component';

const routes: Routes = [
  { path: '', component: BuypageComponent },
  { path: 'positions', component: PositionsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
