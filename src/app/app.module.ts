import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuypageComponent } from './buypage/buypage.component';
import { SidebarDirective } from './sidebar.directive';
import { PositionsComponent } from './positions/positions.component';

// For MDB Angular Pro


@NgModule({
  declarations: [
    AppComponent,
    BuypageComponent,
    SidebarDirective,
    PositionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
