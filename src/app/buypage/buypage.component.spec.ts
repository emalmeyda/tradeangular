import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuypageComponent } from './buypage.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
describe('BuypageComponent', () => {
  let component: BuypageComponent;
  let fixture: ComponentFixture<BuypageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule
      ],
      declarations: [ BuypageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuypageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('unsuccessful submission based on invalid shares', async(() => {
    spyOn(component, 'submit');
    component.stockTicker = "AAPL";
    component.shareCount = -100;
    component.strategy = "sample strategy";
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.whenStable().then(() => {
      expect(component.submit).toHaveBeenCalledTimes(0);
    });
  }));

  it('unsuccessful submission based on invalid ticker', async(() => {
    spyOn(component, 'submit');
    component.stockTicker = null;
    component.shareCount = 100;
    component.strategy = "sample strategy";
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.whenStable().then(() => {
      expect(component.submit).toHaveBeenCalledTimes(0);
    });
  }));

  it('unsuccessful submission based on invalid strategy', async(() => {
    spyOn(component, 'submit');
    component.stockTicker = "AAPL";
    component.shareCount = -100;
    component.strategy = null;
    let button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    
    fixture.whenStable().then(() => {
      expect(component.submit).toHaveBeenCalledTimes(0);
    });
  }));

  it('successful submission based on invalid strategy', async(() => {
    spyOn(component, 'submit');
    component.stockTicker = "AAPL";
    component.shareCount = 100;
    component.strategy = "simple_strategy";
    let button = fixture.debugElement.nativeElement.querySelector('button');

    
    fixture.whenStable().then(() => {
      button.click();
      expect(component.submit).toHaveBeenCalledTimes(1);
    });
  }));
});
