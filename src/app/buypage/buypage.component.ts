import { Component, OnInit } from "@angular/core";
import { DataService } from "../data.service";
import Swal from "sweetalert2";

@Component({
  selector: "app-buypage",
  templateUrl: "./buypage.component.html",
  styleUrls: ["./buypage.component.scss"]
})
export class BuypageComponent implements OnInit {
  stockTicker = "";       //stores stock ticker ex MSFT
  shareCount: number;     //stores amount of shares
  strategy: string;       //strategy picked by user
  strategies = [];        //array of objects
  strategyOptions = [];   //array of available options for specific strategy
  displayToTable = {};    //map of SQL table to Display Name
  userSelections = {};    //map of user selections option=>selection

  //HTML variables for sweet alerts
  title = "<h3>";
  titleEnd = "<h3>";
  body = "<h1 style='font-weight:400'>";
  bodyEnd = "<h1>";
  button = "<h2>";
  buttonEnd = "<h2>";

  constructor(private data: DataService) {}

  ngOnInit() {
    this.data.getStrategies().subscribe((data: any) => {
      this.strategies = data;
      this.strategy = this.strategies[0]["displayName"]; //set the first strategy as default
      this.setMap();
      this.getOptions(this.displayToTable[this.strategy]);
    });
  }

  setMap() {
  //creates map of sql table to hashtable
    for (let i = 0; i < this.strategies.length; i++) {
      this.displayToTable[this.strategies[i]["displayName"]] = this.strategies[i]["strategy"];
    }
  }
  getOptions(strategy: string) {
  //gets options for strategy selected
    this.data.getOptions(strategy).subscribe((data: any) => {
      this.strategyOptions = data;
      this.userSelections = {};
      this.strategyOptions["options"] = this.strategyOptions["options"].split(",");
    });
  }

  validate() {
    if (this.stockTicker == "") {
      this.simpleMessage("Invalid Stock Ticker Name","please try again","error");
      return;
    } else if (this.shareCount == undefined || this.shareCount < 0) {
      this.simpleMessage("Invalid Share Count", "please try again", "error");
      return;
    } else if (this.strategy == undefined || this.strategy == "") {
      this.simpleMessage("Invalid Strategy", "please try again", "error");
      return;
    } else {
      if(Object.keys(this.userSelections).length != this.strategyOptions['options'].length){
        //user missing parameters
        this.simpleMessage("Error, missing user field(s)", "", "error");
        return;
      }
      this.simpleMessage("Successful Strategy Submission", "", "success");
      this.submit();
    }
  }

  submit() {
  //submits user strategy
    this.userSelections["ticker"] = this.stockTicker;
    this.userSelections["shareCount"] = this.shareCount;
    this.userSelections["strategy"] = this.displayToTable[this.strategy];
    this.data.submitStrategy(this.userSelections).subscribe(res => {
      if(res == "success"){
        this.simpleMessage("Successful Strategy Submission", "", "success");
      }else{
        this.simpleMessage(res,"","error");
      }
      this.userSelections = {};
    });
  }

  simpleMessage(title, message, type) {
    //Sweetalert to display invalid message
    return Swal.fire({
      title: this.title + title + this.titleEnd,
      html: this.body + message + this.bodyEnd,
      type: type,
      showConfirmButton: false,
      allowOutsideClick: false,
      timer: 3000
    });
  }
}
