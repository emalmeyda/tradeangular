import { Injectable } from '@angular/core';


import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private baseUrl = 'http://localhost:8081';

  constructor(private http: HttpClient) { }

  getStrategies()
  //gets all available strategies for user
  {
    return this.http.get("/strategy");
  }

  getOptions(strategy : string){
  //gets all available options per strategy
    return this.http.get("/strategyOptions/"+strategy);
  }

  submitStrategy(strategy : Object){
  //submits a strategy for processing
    console.log(strategy);
    return this.http.post('/tradeSubmit', strategy, {responseType: 'text',});
  }

  getPositionStrategies(strategy : string){
  //returns list of positions for each strategy
    //console.log(strategy)
    return this.http.get('/strategy-details/' + strategy);
  }

  stopStrategy(strategyID, strategy){
  //stop strategy from running
    let strategyObject = {}
    strategyObject["strategyID"] = strategyID;
    strategyObject["strategy"] = strategy;
    return this.http.post('/tradeSubmit/stop/', strategyObject, {responseType: 'text',});
  }

}
