import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import 'chartjs-plugin-annotation';
import {DataService} from '../data.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.scss']
})
export class PositionsComponent implements OnInit {
  columnHead = ["Symbol", "Quantity", "Gain/Loss ($)", "Status", "Option" ];      //stores relevant positions of head
  LineChart = [];
  total = 0;
  dataArray = {};
  labelsArray = {};
  plugin = {};
  currentProfitLoss = {};
 
   simpleStrategyProfit = []; 

  strategies = [];        //array of objects
  strategy: string;       //strategy picked by user
  tables = {};            //stores table information
  profitPerTable = {};    //shows profit per strategy
  displayToTable = {};    //map of SQL table to Display Name

  //HTML variables for sweet alerts
  title = "<h3>";
  titleEnd = "<h3>";
  body = "<h1 style='font-weight:400'>";
  bodyEnd = "<h1>";
  button = "<h2>";
  buttonEnd = "<h2>";
  constructor(private dataService : DataService) { }

  ngOnInit() {
    this.dataService.getStrategies().subscribe((data: any) => {
      this.strategies = data;
      this.strategy = this.strategies[0]["displayName"]; //set the first strategy as default
      this.setMap();
    });

    setInterval(() => {
      if(this.strategies){
        for(let i = 0; i < this.strategies.length; i++){
          //console.log("****LOGGING POSITIONS TABLE ******")
          this.getPositionsTable(this.strategies[i].strategy);
          this.profitPerTable[this.strategies[i].strategy] = 0;
        }

        let dateTime = new Date();
        let time = dateTime.getMinutes() + ":" + dateTime.getSeconds();
        //console.log(dateTime.getMinutes() + ":" + dateTime.getSeconds());

        for(let i = 0; i < this.strategies.length; i++){
          if(this.strategies[i].strategy in this.tables)
          {
            for(let j of Object.values(this.tables[this.strategies[i].strategy])){
              this.profitPerTable[this.strategies[i].strategy] += j['profit']
            }
            
          }
          //console.log("*****PROFIT*******", this.profitPerTable[this.strategies[i].strategy])
        }
        if(this.strategy in this.dataArray){
          this.dataArray[this.strategy].push(this.profitPerTable[this.displayToTable[this.strategy]]);
        }

        if(this.strategy in this.labelsArray){
          this.labelsArray[this.strategy].push(this.profitPerTable[this.displayToTable[this.strategy]]);
        }
        this.addData(this.LineChart, time, this.profitPerTable[this.displayToTable[this.strategy]]);
      }
      }, 1000);
    this.changeStrategy(this.strategy);
  }

  addData(chart, label, data) {
    length = 75;
    chart.data.labels.push(label);
    //console.log(chart.data.datasets);

    if( chart.data.labels.length > length){
      chart.data.labels.shift();
    }
    
    chart.data.datasets.forEach((dataset) => {
      this.currentProfitLoss[this.strategy] = data;
      dataset.data.push(data);
      //console.log(dataset.data.length);
      if (dataset.data.length > length ){
        dataset.data.shift();
      }
    });
    chart.update();
  }
  
  
  getPositionsTable(strategy: string) {
  //gets options for strategy selected
    this.dataService.getPositionStrategies(strategy).subscribe((data: any) => {
      //console.log(data);
      this.tables[strategy] = data;
    });
  }

  colorTableRow(stopped){
    if(stopped){
      return "table-danger"
    }
    return "table-success";
  }
  round(number){
    return Math.round(number * 100) / 100
  }

  changeStrategy(strategy){
    this.LineChart = new Chart('lineChart', {
      type: 'line',
      data: {
        labels: this.labelsArray[this.strategy],
        datasets: [{
          label: 'Profit',
          data: this.dataArray[this.strategy],
          fill: false,
          lineTension: 0.2,
          borderColor: "red",
          borderWidth: 1
        }
      
      ]
      },
      options: {
        title: {
          text: "Line Chart",
          display: true,
          responsive: true,
        },
        scales: {
          xAxes: [{
						display: true,
						scaleLabel: {
              display: true,
              maxTicksLimit: 2
            },
            ticks: {
              display: false
            }
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
						
						}
					}]
        },
        
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      annotation: {
        annotations: [{
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-0',
          value: 5,
          borderColor: "green",
          borderWidth: 4,
          label: {
            enabled: false,
            content: 'Test label'
          }
        }]
      }

    });
    
  }

  setMap() {
  //creates map of sql table to hashtable
    for (let i = 0; i < this.strategies.length; i++) {
      this.displayToTable[this.strategies[i]["displayName"]] = this.strategies[i]["strategy"];
    }
  }

  stopStrategy(strategyID, strategy){
    console.log(strategyID, strategy);
    this.dataService.stopStrategy(strategyID, strategy).subscribe(res => {
      if(res == "success"){
        this.simpleMessage("Successful Strategy Stop", "", "success");
      }else{
        this.simpleMessage(res,"","error");
      }
    })
  }

  simpleMessage(title, message, type) {
    //Sweetalert to display invalid message
    return Swal.fire({
      title: this.title + title + this.titleEnd,
      html: this.body + message + this.bodyEnd,
      type: type,
      showConfirmButton: false,
      allowOutsideClick: false,
      timer: 3000
    });
  }
}

